# JakDoczlapie

JakDoczlapie is a custom routing service aimed at programmers. It consists of a browser-based client
and a backend which utilises GraphHopper. It provides several profiles more suited for asocial and obese
pedestrians and bicyclists.

## Deployment considerations

GraphHopper depends on the user to provide it with both map dumps and elevation settings.
As of now these dependencies are declared in `src/main/resources/graphhopper.properties` file.

`mapFile` is an OpenStreetMap dump of the chosen area in .pbf format.

`elevationDir` is the directory in which the elevation data is cached.
It should not be purged for performance reasons,
although **it will download missing data, as needed, AT RUNTIME, when servicing routing requests**.

`graphHopperDir` is a directory where GraphHopper stores its runtime data (spatial indexes and whatnot).
It should be purged each time GraphHopper's configuration is changed (especially in CI config).
