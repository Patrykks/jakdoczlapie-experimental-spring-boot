package app;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GraphHopperSettings extends Properties {
    public static final String DEFAULT_SETTINGS_FILE = "graphhopper.properties";

    public GraphHopperSettings() {
        this(DEFAULT_SETTINGS_FILE);
    }

    public GraphHopperSettings(String settingsFile) {
        InputStream settingsStream = getClass().getClassLoader().getResourceAsStream(settingsFile);

        if (settingsStream == null) {
            throw new RuntimeException("Graphhopper settings file " + settingsFile + " doesn't exist in classpath");
        }

        try {
            load(settingsStream);
        } catch (IOException e) {
            throw new RuntimeException(("Error in loading graphhooper property"));
        }
    }
}
