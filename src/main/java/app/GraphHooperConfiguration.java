package app;

import com.graphhopper.GraphHopper;
import com.graphhopper.reader.dem.CGIARProvider;
import com.graphhopper.reader.dem.ElevationProvider;
import com.graphhopper.routing.util.EncodingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.inject.Inject;
import java.io.File;

@Configuration
public class GraphHooperConfiguration {
    private final static Logger logger = LoggerFactory.getLogger(GraphHooperConfiguration.class);

    @Inject
    private ApplicationContext appContext;

    public GraphHopperSettings ghSettings() {
        return new GraphHopperSettings();
    }

    public ElevationProvider elevationProvider() {
        ElevationProvider elevationProvider = new CGIARProvider();
        elevationProvider.setCacheDir(new File(ghSettings().getProperty("elevationDir")));
        logger.info("ElevationProvider bean initilized correctly");
        return elevationProvider;
    }

    @Bean
    public GraphHopper graphHopperBean() {
        GraphHopper graphHopper = new GraphHopper().forDesktop();
        GraphHopperSettings ghSettings = ghSettings();
        graphHopper.setOSMFile(ghSettings.getProperty("mapFile"));
        graphHopper.setElevationProvider(elevationProvider());
        graphHopper.setGraphHopperLocation(ghSettings.getProperty("graphHopperDir"));

        //graphHopper.setEncodingManager(new EncodingManager("bike"));
        graphHopper.setEncodingManager(new EncodingManager(ghSettings.getProperty("profile")));
        graphHopper.setElevation(Boolean.parseBoolean(ghSettings.getProperty("elevationFlag")));

        // now this can take minutes if it imports or a few seconds for loading
        // of course this is dependent on the area you import
        graphHopper.importOrLoad();
        logger.info("GraphHooper bean initilized correctly");
        return graphHopper;
    }
}
