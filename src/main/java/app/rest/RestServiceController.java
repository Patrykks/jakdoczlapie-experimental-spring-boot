package app.rest;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.PathWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.Locale;

@RestController
public class RestServiceController {

    @Inject
    GraphHopper hopper;

    @RequestMapping("/")
    public List<PathWrapper> index() {
        // simple configuration of the request object, see the GraphHopperServlet classs for more possibilities.
        GHRequest req = new GHRequest(50.0344389, 19.9355802, 50.096714, 19.933106)
                .setWeighting("fastest")
                .setVehicle("bike")
                .setLocale(Locale.ENGLISH);
        GHResponse rsp = hopper.route(req);
        return rsp.getAll();
    }
}
