package app;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.GPXEntry;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.InstructionList;
import com.graphhopper.util.PointList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class AppConfig {

    private final static Logger logger = LoggerFactory.getLogger(AppConfig.class);

    public static void main(String[] args) throws IOException {
        runAppAndLogResult(args);
    }

    private static void runAppAndLogResult(String[] args) throws IOException {
        ApplicationContext ctx = SpringApplication.run(AppConfig.class, args);

        GraphHopper hopper = (GraphHopper) ctx.getBean(GraphHopper.class);

        // simple configuration of the request object, see the GraphHopperServlet classs for more possibilities.
        GHRequest req = new GHRequest(50.0344389, 19.9355802, 50.096714, 19.933106)
                .setWeighting("fastest")
                .setVehicle("bike")
                .setLocale(Locale.ENGLISH);
        GHResponse rsp = hopper.route(req);

        // first check for errors
        if (rsp.hasErrors()) {
            // handle them!
            // rsp.getErrors()
            logger.error("Error: Graphhooper response has" + rsp.getErrors().size() + " errors: " + rsp.getErrors().toString());
            return;
        }

        // use the best path, see the GHResponse class for more possibilities.
        List<PathWrapper> paths = rsp.getAll();

        System.out.println("# paths: " + paths.size());
        for (PathWrapper path : paths) {
            // points, distance in meters and time in millis of the full path
            PointList pointList = path.getPoints();
            for (int i = 0; i < pointList.size(); ++i) {
                System.out.println(pointList.getElevation(i));
            }
            //double distance = path.getDistance();
            //long timeInMs = path.getTime();

            InstructionList il = path.getInstructions();
            // iterate over every turn instruction
            for (Instruction instruction : il) {
                instruction.getDistance();
                System.out.println(instruction.toString());
            }

            // or get the json
            List<Map<String, Object>> iList = il.createJson();

            // or get the result as gpx entries:
            List<GPXEntry> list = il.createGPXList();
        }
    }

}
